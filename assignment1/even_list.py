# for if
def create_even_list_1(n: int) -> list[int]:
    even_list = []
    for i in range(n + 1):
        if i == 0:
            continue
        if i % 2 == 0:
            even_list.append(i)
    return even_list


# for文 range の start, step 使用
def create_even_list_2(n: int) -> list[int]:
    even_list = []
    # range(start, stop, step)
    for i in range(2, n + 1, 2):
        even_list.append(i)
    return even_list


# whileで n が 0 になるまで
def create_even_list_3(n: int) -> list[int]:
    li = []
    # python は 0 だと False
    while n:
        if n % 2 == 0:
            li.append(n)
        n -= 1
    return li[::-1]


# 内包表記
def create_even_list_4(n: int) -> list[int]:
    return [i for i in range(2, n + 1, 2)]


# 内包表記 if
def create_even_list_5(n: int) -> list[int]:
    return [i for i in range(2, n + 1) if i % 2 == 0]


# lambda式
def create_even_list_6(n: int) -> list[int]:
    li = [i for i in range(2, n + 1)]
    # filter 第一引数の条件に合う(True)値を返す
    even_list = list(filter(lambda x: x % 2 == 0, li))
    return even_list


# 計算量を減らす?
def create_even_list_7(n: int) -> list[int]:
    # pythonは // で切り捨て
    return [(i + 1) * 2 for i in range(n // 2)]


# 両端から埋めていく
def create_even_list_8(n: int) -> list[int]:
    lower = []
    upper = []

    # 奇数ならn以下の最大の偶数に変換
    if n % 2 == 1:
        n -= 1

    for i in range(n):
        if i in upper:
            break
        if i % 2 == 0:
            lower.append(i)
            # n を2で割った値が偶数用
            if i != n - i:
                upper.append(n - i)
    # lowerの先頭の数字 0 を除く
    return lower[1:] + upper[::-1]


# ジェネレーター使用
def create_even_list_9(n: int) -> list[int]:
    return [i for i in even_generator(n)]


def even_generator(n):
    for i in range(n // 2):
        yield (i + 1) * 2


# 一番短い？
def create_even_list_10(n: int) -> list[int]:
    return list(range(2, n + 1, 2))


def main():
    fs = [
        create_even_list_1,
        create_even_list_2,
        create_even_list_3,
        create_even_list_4,
        create_even_list_5,
        create_even_list_6,
        create_even_list_7,
        create_even_list_8,
        create_even_list_9,
        create_even_list_10,
    ]

    # 標準入力を受け取る
    n = int(input())

    failed = False
    for i, f in enumerate(fs):
        if list(range(2, n + 1, 2)) != f(n):

            print(f"f{i + 1} failed.")
            print(f"  expected: {list(range(2, n + 1, 2))}")
            print(f"  actual  : {f(n)}\n")
            failed = True

    if not failed:
        print("Successfully!")


if __name__ == "__main__":
    main()
