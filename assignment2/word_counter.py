class WordCounter:
    def __init__(self, document):
        self.set_document(document)

    def set_document(self, document):
        self.__document = document
        self.lines = self.__count_lines()
        self.words = self.__count_words()
        self.characters = self.__count_characters()

    def get_document(self):
        return self.__document

    def __count_lines(self):
        return self.__document.count("\n") + 1

    def __count_words(self):
        document = self.__document.strip()
        return document.count(" ") + document.count("\n") + 1

    def __count_characters(self):
        return (
            len(self.__document)
            - self.__document.count(" ")
            - self.__document.count("\n")
        )


# document = "Today is fine."

# word_counter = WordCounter(document)

# assert word_counter.get_document() == document
# assert word_counter.lines == 1  # 正しい行数は1行
# assert word_counter.words == 3  # 正しい単語数は3つ
# assert word_counter.characters == 12


# # 2つめのテスト
# document = """Hello, world.
# This is python world."""  # 改行を含む文字列
# word_counter.set_document(document)  # 英文の変更

# assert word_counter.get_document() == document
# assert word_counter.lines == 2  # 正しい行数は2行
# assert word_counter.words == 6  # 正しい単語数は6つ
# assert word_counter.characters == 30  # 正しい文字数は30文字（スペース・改行文字を除く）


# # 3つめのテスト
# document = """
#  Hello, world.
# This is python world.
#  """  # 改行を含む文字列
# word_counter.set_document(document)  # 英文の変更

# assert word_counter.get_document() == document
# assert word_counter.lines == 4  # 正しい行数は4行
# assert word_counter.words == 6  # 正しい単語数は6つ
# assert word_counter.characters == 30  # 正しい文字数は30文字（スペース・改行文字を除く）

# print("テスト完了!")
