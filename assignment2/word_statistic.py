import re
from collections import defaultdict


class WordStatistic:
    def __init__(self, document) -> None:
        self.__document = document
        self.__lines = self.__split_by_lines()
        self.__words = self.__split_by_words()

    def get_document(self) -> str:
        """
        クラスの英文を返す関数

        Args:
            None
        Returns:
            str: インスタンス生成時に入力された英文
        """
        return self.__document

    def get_lines(self) -> list[str]:
        """
        クラスの英文を行ごとに分割した配列を返す関数

        Arags:
            None
        Returns:
            list[str]: 英文を行ごとに分割した配列
        """
        return self.__lines

    def get_words(self) -> list[str]:
        """
        クラスの英文を英単語ごとに分割した配列を返す関数

        Arags:
            None
        Returns:
            list[str]: 英文を英単語ごとに分割した出現順の配列
        """
        return self.__words

    def get_number_of_lines(self) -> int:
        """
        クラスの英文の行数を返す関数

        Args:
            None
        Returns:
            int: 英文の行数（改行文字の数 + 1）
        """
        return len(self.__lines)

    def get_number_of_words(self) -> int:
        """
        クラスの英文の英単語の数を返す関数

        Args:
            None
        Returns:
            int: 英文の単語の数（重複あり）
        """
        return len(self.__words)

    def get_frequency_of_word_usage(self) -> str:
        """
        クラスの英文の単語を使用頻度順に整形した文字列を返す関数

        Args:
            None
        Returns:
            str: 単語の使用頻度順（同数の場合はアルファベット順）の集計結果
        """
        return self.__format_data(self.__count_frequency_of_word_usage())

    def get_frequency_of_alphabet(self, distinction=False) -> str:
        """
        クラスの英文に含まれるアルファベットの使用頻度をまとめた結果を整形した文字列を返す関数
        デフォルトでは大文字、小文字を区別しない

        Args:
            distinction: bool 大文字、小文字を区別する(True)
        Returns:
            str: アルファードの使用頻度順（大文字、小文字区別 選択）の集計結果
        """
        if distinction:
            return self.__format_data(self.__count_frequency_of_alphabet_usage())
        else:
            return self.__format_data(
                self.__count_frequency_of_alphabet_usage_wihtout_distinction()
            )

    def __split_by_lines(self) -> list[str]:
        """
        クラスの英文の行ごとに分割する関数

        Returns:
            list[str]: 英文を改行文字で分割したリスト
        """
        return re.split("\n", self.__document)

    def __split_by_words(self) -> list[str]:
        """
        クラスの英文の単語ごとに分割する関数

        Returns:
            list[str]: 英文に含まれる単語リスト
        """
        return re.findall("[a-zA-Z]+", self.__document)

    def __count_frequency_of_word_usage(self) -> list[tuple[str, int]]:
        """
        英文に含まれる英単語の使用頻度を集計する関数

        Returns:
            list[tuple[str, int]]: 英単語ごとの集計結果
            ex. [("hello", 2), ("world", 2), ..., ("hi", 1)]
        """
        word_dict: dict[str, int] = defaultdict(int)
        for word in self.__words:
            word_dict[word.lower()] += 1
        return sorted(word_dict.items(), key=lambda x: (-x[1], x[0]))

    def __count_frequency_of_alphabet_usage(self) -> list[tuple[str, int]]:
        """
        英文に含まれるアルファベットの使用頻度を集計する関数

        Returns:
            list[tuple[str, int]]: アルファベットごとの集計結果
            ex. [("A", 2), ("B", 0), ..., ("z", 1)]
        """
        alphabet_dict = dict()

        for i in range(26):
            alphabet_dict[chr(i + ord("A"))] = 0
            alphabet_dict[chr(i + ord("a"))] = 0

        for alphabet in re.findall("[a-zA-Z]", self.__document):
            alphabet_dict[alphabet] += 1

        return sorted(alphabet_dict.items())

    def __count_frequency_of_alphabet_usage_wihtout_distinction(
        self,
    ) -> list[tuple[str, int]]:
        """
        英文に含まれるアルファベット(大文字、小文字を区別しない)の使用頻度を集計する関数

        Returns:
            list[tuple[str, int]]: アルファベットごとの集計結果
            ex. [("A", 2), ("B", 0), ..., ("Z", 1)]
        """
        alphabet_dict = dict()

        for i in range(26):
            alphabet_dict[chr(i + ord("A"))] = 0

        for alphabet in re.findall("[a-zA-Z]", self.__document):
            alphabet_dict[alphabet.upper()] += 1

        return sorted(alphabet_dict.items())

    def __format_data(self, data: list[tuple[str, int]]) -> str:
        """集計データを見やすい形に整形する関数

        Args:
            data (list[tuple[str, int]]): 集計データ

        Returns:
            str: 集計データを文字列に変形したデータ
        """
        max_key_len = max([len(key) for key, _ in data])
        max_val_len = max([len(str(val)) for _, val in data])
        return "\n".join(
            list(map(lambda x: f"{x[0]:>{max_key_len}} -> {x[1]:>{max_val_len}}", data))
        )


if __name__ == "__main__":
    document = """Hello, world.
    This is python world."""

    ws = WordStatistic(document)

    print(ws.get_frequency_of_alphabet())

    print(ws.get_frequency_of_word_usage())

    ws.get_document()
