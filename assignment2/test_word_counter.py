import unittest

from word_counter import WordCounter

document = """Hello, world.
This is python world."""


class TestWordCounter(unittest.TestCase):
    def setUp(self):
        print("setup")
        self.wordCounter = WordCounter(document)

    def test_case_1(self):

        self.assertEqual(self.wordCounter.get_document(), document)
        self.assertEqual(self.wordCounter.lines, 2)
        self.assertEqual(self.wordCounter.words, 7)
        self.assertEqual(self.wordCounter.characters, 30)

    def hoge(self):
        self.assertEqual(1, 3)

    def tearDown(self):
        print("tearDown")
        del self.wordCounter


if __name__ == "__main__":
    unittest.main()
