import unittest

from word_statistic import WordStatistic


class WordStatisticTestCase1(unittest.TestCase):
    def setUp(self) -> None:
        self.__document = """Hello, world.
This is python world."""
        self.wordStatistic = WordStatistic(self.__document)

    def test_is_same_document(self):
        self.assertEqual(self.wordStatistic.get_document(), self.__document)

    def test_is_correct_counter_words(self):
        self.assertEqual(self.wordStatistic.get_number_of_words(), 6)

    def test_is_correct_counter_lines(self):
        self.assertEqual(self.wordStatistic.get_number_of_lines(), 2)

    def test_is_correct_word(self):
        actual = " world -> 2\n hello -> 1\n    is -> 1\npython -> 1\n  this -> 1"
        self.assertEqual(self.wordStatistic.get_frequency_of_word_usage(), actual)

    def tearDown(self) -> None:
        del self.wordStatistic


class WordStatisticTestCase2(unittest.TestCase):
    def setUp(self) -> None:
        self.__document = """
 Hello, world.
This is python world.
 """
        self.wordStatistic = WordStatistic(self.__document)

    def test_is_same_document(self):
        self.assertEqual(self.wordStatistic.get_document(), self.__document)

    def test_is_correct_counter_words(self):
        self.assertEqual(self.wordStatistic.get_number_of_words(), 6)

    def test_is_correct_counter_lines(self):
        self.assertEqual(self.wordStatistic.get_number_of_lines(), 4)

    def test_is_correct_word(self):
        actual = " world -> 2\n hello -> 1\n    is -> 1\npython -> 1\n  this -> 1"
        self.assertEqual(self.wordStatistic.get_frequency_of_word_usage(), actual)

    def tearDown(self) -> None:
        del self.wordStatistic


class WordStatisticTestCase3(unittest.TestCase):
    def setUp(self) -> None:
        self.__document = """
Hoge Hoge \n\n\n
 """
        self.wordStatistic = WordStatistic(self.__document)

    def test_is_same_document(self):
        self.assertEqual(self.wordStatistic.get_document(), self.__document)

    def test_is_correct_counter_words(self):
        self.assertEqual(self.wordStatistic.get_number_of_words(), 2)

    def test_is_correct_counter_lines(self):
        self.assertEqual(self.wordStatistic.get_number_of_lines(), 6)

    def test_is_no_distination(self):
        data = self.wordStatistic.get_frequency_of_alphabet()
        length = len(data.split("\n"))
        self.assertEqual(length, 26)

    def test_is_distination(self):
        data = self.wordStatistic.get_frequency_of_alphabet(True)
        length = len(data.split("\n"))
        self.assertEqual(length, 52)

    def tearDown(self) -> None:
        del self.wordStatistic


if __name__ == "__main__":
    unittest.main()
